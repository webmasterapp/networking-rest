<?php

declare(strict_types=1);

namespace MasterApp\Networking;
use Nette\Http\IRequest;

/**
 * Class Utils
 * Contains some useful functions
 * @package MasterApp\Networking;
 */
class Utils {

    // CloudFlare IP header
    private const cloudFlareIPHeader = 'cf-connecting-ip';

    // CloudFlare Country header
    private const cloudFlareCountryHeader = 'cf-ipcountry';

    /**
     * Try to get client IP
     * @param IRequest $request
     * @return string
     */
    public static function getClientIp(IRequest $request): string {
        return $request->getHeader(self::cloudFlareIPHeader)
            ?? $request->getRemoteAddress()
            ?? 'none';
    }

    public static function getClientCountry(IRequest $request): string {
        return $request->getHeader(self::cloudFlareCountryHeader) ?? 'unknown';
    }
}
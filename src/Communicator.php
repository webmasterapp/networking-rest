<?php

declare(strict_types=1);

namespace MasterApp\Networking;
use CurlHandle;
use JsonException;
use MasterApp\Networking\Exceptions\RestCommunicationImplementationException;
use MasterApp\Networking\Exceptions\RestCommunicationResponseInternalException;
use MasterApp\Networking\Exceptions\RestCommunicationResponseUnauthorizedException;
use MasterApp\Networking\Exceptions\RestCommunicationResponseWarningException;
use MasterApp\Networking\Exceptions\RestCommunicationSystemException;
use Netpromotion\Profiler\Profiler;
use Nette\Application\AbortException;
use Nette\Caching\Cache;
use Nette\Caching\Storage;
use Nette\Http\IRequest;
use Tracy\Debugger;

/**
 * Class Communicator
 * @package MasterApp\Networking
 */
class Communicator {

    // Contains Basic Auth Token [DI]
    public ?string $basicAuth = null;

    // Regex terms which should be skipped from reporting as error [DI]
    public ?array $ignoredErrorRegexes = null;

    // If this flag is enabled - the cache is disabled [PUBLIC]
    public bool $freshUpdate = false;

    // Contains presenter [PUBLIC]
    public ?BasicRestApiPresenter $presenterObject = null;

    // If used by test case or auth [PUBLIC]
    public bool $presenterUnattached = false;

    // Contains User ID from presenter [INIT/PUBLIC]
    public ?string $userId = null;

    // Contains Bearer Token [INIT/PUBLIC]
    public ?string $bearerToken = null;

    // Contains base endpoint URL [DI]
    private string $domain;

    // If enabled logging is set and cache is disabled [DI]
    private bool $debugModeEnabled;

    // Tell if API don't use any auth [DI]
    private bool $unauthorizedMode;

    // Where to redirect on unauthorized [DI]
    private string $unauthorizedLink;

    // Where to redirect on token expire [DI]
    private string $loginLink;

    // Contains request parameters [PARAM]
    private SendRequestObjectParameters $requestParameters;

    // Contains current locale [INIT]
    private ?string $languageISO = null;

    // Current debug object [INIT]
    private DebugObject $debugObject;

    // Current curl object [INIT]
    private CurlHandle $curlObject;

    // Current curl headers [INIT]
    private array $curlHeaders = [];

    // Current cache key [INIT]
    private ?string $cacheKey = null;

    // Current cURL result data [INIT]
    private string|null $curlData = null;

    // Output result [INIT]
    private ?array $result = null;

    // Encoded entity [INIT]
    private ?string $encodedEntity = null;

    // Setters and constructor
    public function __construct(private readonly Logger $logger, private readonly Storage $cache, private readonly ?IRequest $request = null) {}

    /** @noinspection PhpUnused */
    public function setDomain(string $domain): void { $this->domain = $domain; }

    /** @noinspection PhpUnused */
    public function setBasicAuth(?string $basicAuth): void { $this->basicAuth = $basicAuth; }

    /** @noinspection PhpUnused */
    public function setDebug(bool $debugMode): void { $this->debugModeEnabled = $debugMode; }

    /** @noinspection PhpUnused */
    public function setUnauthorizedMode(bool $unauthorizedMode): void { $this->unauthorizedMode = $unauthorizedMode; }

    /** @noinspection PhpUnused */
    public function setUnauthorizedLink(string $unauthorizedLink): void { $this->unauthorizedLink = $unauthorizedLink; }

    /** @noinspection PhpUnused */
    public function setLoginLink(string $loginLink): void { $this->loginLink = $loginLink; }

    /** @noinspection PhpUnused */
    public function setIgnoredRegexes(?array $ignoredRegex): void { $this->ignoredErrorRegexes = $ignoredRegex; }

    /**
     * Send the request to the API
     * @param SendRequestObjectParameters $parameters
     * @return array|null
     * @throws AbortException
     * @throws RestCommunicationImplementationException
     * @throws RestCommunicationResponseUnauthorizedException
     * @throws RestCommunicationResponseInternalException
     * @throws RestCommunicationResponseWarningException
     * @throws RestCommunicationSystemException
     * @noinspection PhpUnused
     */
    public function sendRequestObject(SendRequestObjectParameters $parameters): ? array {

        $this->requestParameters = $parameters;
        $this->initializeDebugObject();
        $this->checkPreConditions();
        $this->initializePropertiesFromPresenter();
        $this->encodeEntityToPostFieldsFormat();
        $cachedData = $this->initCacheAndTryToLoadResult();

        if ($cachedData !== null) {
            $this->debug();
            return $cachedData;
        }

        $this->initializeCurlObject();
        $this->addPostFieldsToRequest();
        $this->addAuthFieldsToRequest();
        $this->addLangFieldsToRequest();
        $this->addRemoteInfoFieldsToRequest();
        $this->sendRequest();
        $this->updateCache();
        $this->processErrors();
        $this->debug();
        return $this->result;
    }

    /**
     * Try to get debug object
     * @return DebugObject
     * @noinspection PhpUnused
     */
    public function getDebugObject(): DebugObject {
        return $this->debugObject;
    }

    /**
     * Check input conditions
     * @throws RestCommunicationImplementationException
     */
    private function checkPreConditions(): void {

        if ($this->presenterObject === null && ! $this->presenterUnattached) {
            throw new RestCommunicationImplementationException($this->debugObject,'Presenter unattached. Fix or use presenterUnattached param!');
        }
        if (! $this->requestParameters->useBasicAuthFromConfig && $this->basicAuth === null) {
            throw new RestCommunicationImplementationException($this->debugObject, 'Basic auth requested but not set in config!');
        }
        if (! $this->presenterUnattached && $this->requestParameters->useBearerTokenFromPresenter
            && ($this->presenterObject === null || ! $this->presenterObject->getUser()->isLoggedIn())) {
            throw new RestCommunicationImplementationException($this->debugObject, 'Bearer from presenter requested but not logged in!');
        }
        if ($this->requestParameters->useBearerTokenFromPresenter && $this->requestParameters->useBasicAuthFromConfig) {
            throw new RestCommunicationImplementationException($this->debugObject, 'Bearer and Basic auth requested both!');
        }
        if ($this->requestParameters->dontProcessUnauthorizedException === false && $this->presenterUnattached === false && $this->presenterObject === null) {
            throw new RestCommunicationImplementationException($this->debugObject, 'Process unauthorized with presenter attached but is null!');
        }
    }

    /**
     * Initialize cURL resource object
     * @throws RestCommunicationSystemException
     */
    private function initializeCurlObject(): void {

        $curl = curl_init($this->domain . $this->requestParameters->endpoint);
        if ($curl === false) { throw new RestCommunicationSystemException($this->debugObject,'Impossible to init cURL resource!'); }
        $this->curlObject = $curl;
    }

    /**
     * Initialize User ID and User Bearer Token
     * Check if user is logged and try to get base info
     * @throws RestCommunicationImplementationException
     */
    private function initializePropertiesFromPresenter(): void {

        // Language
        if ($this->presenterUnattached) { return; }
        if ($this->presenterObject === null) {
            throw new RestCommunicationImplementationException($this->debugObject, 'Presenter not unattached but used!');
        } $this->languageISO = $this->presenterObject->translator->getLocale();

        // User ID
        $user = $this->presenterObject->getUser();
        if (! $user->isLoggedIn()) { return; }
        $this->userId = $user->getId() !== null ? (string) $user->getId() : null;

        // Token
        $identity = $user->getIdentity();
        if ($identity === null) { return; }
        $this->debugObject->identity = $identity;
        $data = $identity->getRoles();
        if (empty($data[0])) { return; }
        $this->bearerToken = $data[0];
    }

    /**
     * Initialize debug object
     * Fill basic class variables into some object wrapper
     */
    private function initializeDebugObject(): void {

        $debugObject = new DebugObject();
        $debugObject->logger = $this->logger;
        $debugObject->parameters = $this->requestParameters;
        $debugObject->request = $this->request;
        $debugObject->communicatorDebug->presenterObjectClass = $this->presenterObject !== null ? get_class($this->presenterObject) : null;
        $debugObject->communicatorDebug->userId = $this->userId;
        $debugObject->communicatorDebug->domain = $this->domain;
        $debugObject->communicatorDebug->debugModeEnabled = $this->debugModeEnabled;
        $debugObject->communicatorDebug->freshUpdate = $this->freshUpdate;
        $debugObject->communicatorDebug->bearerToken = $this->bearerToken;
        $debugObject->communicatorDebug->languageISO = $this->languageISO;
        $debugObject->communicatorDebug->unauthorizedMode = $this->unauthorizedMode;
        $this->debugObject = $debugObject;
    }

    /**
     * If type is POST, parse body query into post format
     * Initialize cURL object to post body in POST request
     */
    private function addPostFieldsToRequest(): void {

        // Init headers
        $this->curlHeaders = [];
        curl_setopt($this->curlObject, CURLOPT_CUSTOMREQUEST, $this->requestParameters->type->value);
        $this->curlHeaders[] = 'Content-Type: ' . $this->requestParameters->contentType;

        // Init send values
        if ($this->requestParameters->type === RequestTypes::GET) { return; }
        if ($this->encodedEntity === null) { return; }
        curl_setopt($this->curlObject, CURLOPT_POSTFIELDS, $this->encodedEntity);
        $this->curlHeaders[] = 'Content-Length: ' . strlen($this->encodedEntity);
    }

    /**
     * Initialize authorization part of request
     * If requested append Bearer token into request
     */
    private function addAuthFieldsToRequest(): void {

        // Use basic auth from DI config
        if ($this->basicAuth !== null && $this->requestParameters->useBasicAuthFromConfig) {
            curl_setopt($this->curlObject, CURLOPT_USERPWD, $this->basicAuth);
            return;
        }

        if ($this->unauthorizedMode) { return; }
        $bearerToken = $this->requestParameters->bearerToken ?? $this->bearerToken;
        if ($bearerToken === null) { return; }
        $this->curlHeaders[] = 'Authorization: Bearer ' . $bearerToken;
    }

    /**
     * Initialize language header
     * If requested send accept language header
     */
    private function addLangFieldsToRequest(): void {

        if ($this->requestParameters->dontSendLanguageInRequest) { return; }
        $language = $this->requestParameters->languageISO ?? $this->languageISO;
        $this->curlHeaders[] = "Accept-Language: {$language}";
    }

    /**
     * Initialize request info into headers
     * Look into request and send info in headers
     * @throws RestCommunicationSystemException
     */
    private function addRemoteInfoFieldsToRequest(): void {

        if ($this->requestParameters->dontSendRequestInfoInRequest) { return; }
        if ($this->request === null) { return; }

        $parameters = is_array($this->request->getQuery()) ? $this->request->getQuery() : null;
        try { $parameters = $parameters !== null ? json_encode($parameters, JSON_THROW_ON_ERROR) : null; }
        catch (JsonException $e) { throw new RestCommunicationSystemException($this->debugObject,'Impossible to encode request parameters!', $e); }

        $url = $this->request->getUrl()->getAbsoluteUrl() === 'http:/' ? null : $this->request->getUrl()->getAbsoluteUrl();
        $this->curlHeaders[] = 'Connecting-IP: ' . Utils::getClientIp($this->request);
        $this->curlHeaders[] = 'Connecting-Country: ' . Utils::getClientCountry($this->request);
        $this->curlHeaders[] = 'Connecting-Agent: ' . ($this->request->getHeader('User-Agent') ?? 'Unknown');
        $this->curlHeaders[] = 'Connecting-Host: ' . ($this->request->getRemoteHost() ?? 'Unknown');
        $this->curlHeaders[] = 'Connecting-Language: ' . ($this->request->getHeader('Accept-language') ?? 'Unknown');
        $this->curlHeaders[] = 'Connecting-Async: ' . ($this->request->isAjax() ? 'Yes' : 'No');
        $this->curlHeaders[] = 'Connecting-URL: ' . ($url ?? 'Unknown');
        $this->curlHeaders[] = 'Connecting-Parameters: ' . ($parameters ?? 'Unknown');
    }

    /**
     * Initialize cache key and try to load data from cache
     * Save cache key into property and return array with data or null
     * @return array|null
     * @throws RestCommunicationSystemException
     */
    private function initCacheAndTryToLoadResult(): ? array {

        // Create key
        $callerFunction = debug_backtrace()[2]['function'];
        $userKey = $this->userId ?? 'user';
        $languageKey = $this->languageISO ?? 'language';
        $endpointKey = hash('md5', $this->requestParameters->endpoint);
        $entityKey = $this->encodedEntity ?? 'entity';
        $typeKey = $this->requestParameters->type->value;
        $locationKey = __DIR__;

        // Remove specific variable parts of key
        if ($this->requestParameters->disableCachePerUser) { $userKey = 'user'; }
        if ($this->requestParameters->languageISO) { $languageKey = 'language'; }

        // Save cache key
        $this->cacheKey = "{$locationKey}-{$typeKey}-{$callerFunction}-{$userKey}-{$languageKey}-{$endpointKey}-{$entityKey}";
        $this->debugObject->cacheDebug->cachedKey = $this->cacheKey;

        // Disable cache if requested
        if ($this->requestParameters->validSeconds === null) { return null; }
        if ($this->debugModeEnabled) { return null; }
        if ($this->presenterUnattached) { return null; }

        // Load from cache
        $storedData = $this->cache->read($this->cacheKey);
        $this->debugObject->cacheDebug->result = $storedData;
        if ($storedData === null) { return null; }
        try { $decoded = json_decode($storedData, true, 512, JSON_THROW_ON_ERROR); }
        catch (JsonException $e) { throw new RestCommunicationSystemException($this->debugObject,'Impossible to decode cached entity!', $e); }
        $this->debugObject->cacheDebug->cached = true;

        // Parse data
        return $decoded;
    }

    /**
     * Initialize cURL
     * Send request to BE using cURL and process HTTP codes
     * @throws AbortException
     * @throws RestCommunicationResponseInternalException
     * @throws RestCommunicationResponseUnauthorizedException
     * @throws RestCommunicationResponseWarningException
     * @throws RestCommunicationSystemException
     * @throws RestCommunicationImplementationException
     */
    private function sendRequest(): void {

        // Initialize instance of cURL
        $this->debugObject->curlDebug->curlHeaders = $this->curlHeaders;
        curl_setopt($this->curlObject, CURLOPT_HTTPHEADER, $this->curlHeaders);
        curl_setopt($this->curlObject, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curlObject, CURLOPT_TIMEOUT, $this->requestParameters->curlTimeout);
        curl_setopt($this->curlObject, CURLOPT_CONNECTTIMEOUT, $this->requestParameters->curlTimeout);

        // Run
        if ($this->debugModeEnabled) { Profiler::start($this->requestParameters->endpoint); }
        $cURLResult = curl_exec($this->curlObject);
        if ($this->debugModeEnabled) { Profiler::finish($this->requestParameters->endpoint); }
        $cURLCode = curl_getinfo($this->curlObject, CURLINFO_HTTP_CODE);
        $cURLError = curl_errno($this->curlObject) !== 0 ? curl_error($this->curlObject) : null;

        // Save debug
        $this->debugObject->curlDebug->code = $cURLCode;
        $this->debugObject->curlDebug->result = $cURLResult;
        $this->debugObject->curlDebug->curlError = $cURLError;
        $this->debugObject->curlDebug->curlTotalTime = (round(curl_getinfo($this->curlObject, CURLINFO_TOTAL_TIME) * 1000)) . 'ms';
        $this->debugObject->curlDebug->curlFirstTime = (round(curl_getinfo($this->curlObject, CURLINFO_STARTTRANSFER_TIME) * 1000)) . 'ms';
        curl_close($this->curlObject);

        // Internal error
        $exp = match ($cURLCode) {
            0 => new RestCommunicationResponseInternalException($this->debugObject, null, $cURLError ?? 'Backend communication cURL error!', 0),
            500 => new RestCommunicationResponseInternalException($this->debugObject, null,'Backend communication 500 error!', 500),
            default => null,
        }; if ($exp !== null) { throw $exp; }

        // Wrong payload
        if (is_bool($cURLResult)) {
            throw new RestCommunicationResponseInternalException($this->debugObject, null, 'Backend communication payload empty!', $cURLCode);
        } $this->curlData = $cURLResult;

        // Process payload
        $this->processResult();

        // Correct
        if ($cURLCode === 200 || $cURLCode === 201) { return; }

        // Bad request
        $isBadRequestCode = $cURLCode === 404 || $cURLCode === 400;
        if ($this->requestParameters->dontThrowExceptionOnBadRequest && $isBadRequestCode) {
            return;
        }

        // Unauthorized
        if ($cURLCode === 403 || $cURLCode === 401) {

            if ($this->requestParameters->dontProcessUnauthorizedException === true || $this->presenterUnattached === true) {
                throw new RestCommunicationResponseUnauthorizedException($this->debugObject);
            } $this->debug();

            if ($this->presenterObject === null) {
                throw new RestCommunicationImplementationException($this->debugObject, 'Presenter not unattached but used!');
            }

            if ($this->presenterObject->getUser()->isLoggedIn()) { $this->presenterObject->getUser()->logout(true); }
            $this->presenterObject->redirect($cURLCode === 403 ? $this->unauthorizedLink : $this->loginLink);
        }

        // Other error
        throw new RestCommunicationResponseWarningException($this->debugObject);
    }

    /**
     * Update cache
     * Delete requested tags and create new entry with key and tags
     * @throws RestCommunicationImplementationException
     */
    private function updateCache(): void {

        // Delete
        $deleteTags = $this->requestParameters->invalidateTags ?? [];
        if ($this->requestParameters->invalidateUserTags !== null && $this->userId === null) {
            throw new RestCommunicationImplementationException($this->debugObject, 'Invalidate user tags but not user found!');
        }

        // Delete user
        if ($this->requestParameters->invalidateUserTags !== null) {
            $_this = $this;
            $deleteTags = array_merge($deleteTags, array_map(static function(string $tag) use ($_this) {
                return "{$tag}-{$_this->userId}"; }, $this->requestParameters->invalidateUserTags));
        }

        // Process delete
        $this->debugObject->cacheDebug->deleteTags = $deleteTags;
        $this->cache->clean([Cache::TAGS => $deleteTags]);

        // Skip if requested
        if ($this->requestParameters->validSeconds === null) { return; }
        if ($this->curlData === null) { return; }

        // Check cache key
        if ($this->cacheKey === null) {
            throw new RestCommunicationImplementationException($this->debugObject, 'Caching requested, but key is null!');
        }

        // Init tags and save
        $callerFunction = debug_backtrace()[2]['function'];
        $saveTags = [$callerFunction];
        if ($this->userId !== null) { $saveTags[] = $this->userId; $saveTags[] = "{$callerFunction}-{$this->userId}"; }
        if ($this->requestParameters->additionalCacheTags !== null) { $saveTags = array_merge($saveTags, $this->requestParameters->additionalCacheTags); }
        $this->debugObject->cacheDebug->saveTags = $saveTags;
        $this->cache->write($this->cacheKey, $this->curlData, [Cache::EXPIRE => $this->requestParameters->validSeconds, Cache::TAGS => $saveTags]);
    }

    /**
     * Process string response from cURL and parse it
     * @throws RestCommunicationImplementationException
     */
    private function processResult(): void {

        // Check API result is set
        if ($this->curlData === null) {
            throw new RestCommunicationImplementationException($this->debugObject, 'Decoding of API result requested, but is null!');
        }

        // Decode json into array
        try { $resultDecoded = json_decode($this->curlData, true, 512, JSON_THROW_ON_ERROR); }
        catch (JsonException) { $resultDecoded = null; }
        $this->debugObject->curlDebug->resultArray = $resultDecoded;
        if ($this->requestParameters->dontProcessResultData) { return; }
        $this->result = $resultDecoded;

        // Decode as raw result
        if ($this->result === null && $this->requestParameters->returnRawDataAsString) {
            $this->result = ['raw' => $this->curlData];
        }
    }

    /**
     * Process errors
     * @throws RestCommunicationResponseUnauthorizedException
     * @throws RestCommunicationResponseWarningException
     */
    private function processErrors(): void {

        // Search for errors
        if (! isset($this->result['errors']) && ! isset($this->result['error'])) { return; }

        // Unauthorized simple
        if (isset($this->result['error']) && $this->result['error'] === 'Unauthorized' && ! $this->requestParameters->dontProcessUnauthorizedException) {
            throw new RestCommunicationResponseUnauthorizedException($this->debugObject);
        }

        // Unauthorized multi
        if (isset($this->result['errors']) && ! $this->requestParameters->dontProcessUnauthorizedException) {
            foreach ($this->result['errors'] as $error) {
                if (isset($error['extensions']['errorType']) && $error['extensions']['errorType'] === 'AUTHENTICATION_ERR') {
                    throw new RestCommunicationResponseUnauthorizedException($this->debugObject);
                }
            }
        }

        // Warning
        throw new RestCommunicationResponseWarningException($this->debugObject);
    }

    /**
     * Debug on script finish
     * We want to show in bar
     * @throws RestCommunicationSystemException
     */
    private function debug(): void {

        $this->logger->generateLogFromDebugObject($this->debugObject);
        if (! $this->debugModeEnabled) { return; }
        Debugger::barDump($this->debugObject, $this->requestParameters->endpoint);
    }

    /**
     * Convert entity object to post body
     * @throws RestCommunicationSystemException
     */
    private function encodeEntityToPostFieldsFormat(): void {

        if ($this->requestParameters->entity === null && $this->requestParameters->entityList === null) { return; }
        if ($this->requestParameters->contentType === 'application/x-www-form-urlencoded') {
            $this->encodedEntity = http_build_query((array) $this->requestParameters->entity);
            return;
        }

        $toEncode = $this->requestParameters->entityList ?? $this->requestParameters->entity;
        if ($toEncode === null) { return; }
        try { $this->encodedEntity = json_encode($toEncode, JSON_THROW_ON_ERROR); } catch (JsonException $e) {
            throw new RestCommunicationSystemException($this->debugObject,  'Impossible to encode the entity', $e);
        }
    }
}
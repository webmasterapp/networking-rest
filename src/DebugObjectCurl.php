<?php

declare(strict_types=1);

namespace MasterApp\Networking;

/**
 * Class DebugObjectCurl
 * @package App\Models\Logger
 */
class DebugObjectCurl {

    // CURL result code
    public ?int $code = null;

    // CURL result
    public string|bool|null $result = null;

    // CURL error
    public ?string $curlError = null;

    // CURL total time
    public ?string $curlTotalTime = null;

    // CURL first time
    public ?string $curlFirstTime = null;

    // CURL headers
    public ?array $curlHeaders = null;

    // CURL result as array
    public ?array $resultArray = null;
}

<?php

declare(strict_types=1);

namespace MasterApp\Networking;
use Contributte\Translation\Translator;
use Nette\Application\UI\Presenter;

/**
 * Class BasicGraphQLApiPresenter
 * @package GraphQLFrontApi
 */
abstract class BasicRestApiPresenter extends Presenter {

    public Translator $translator;

    public function __construct(Translator $translator) {
        parent::__construct();
        $this->translator = $translator;
    }
}
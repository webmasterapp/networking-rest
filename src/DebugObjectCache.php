<?php

declare(strict_types=1);

namespace MasterApp\Networking;

/**
 * Class DebugObjectCache
 * @package App\Models\Logger
 */
class DebugObjectCache {

    // If cached
    public ?bool $cached = null;

    // Cache key
    public ?string $cachedKey = null;

    // Cache result
    public ?string $result = null;

    // Cache tags used
    public ?array $saveTags = null;

    // Cache tags for user used
    public ?array $deleteTags = null;
}

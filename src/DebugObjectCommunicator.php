<?php

declare(strict_types=1);

namespace MasterApp\Networking;

/**
 * Class DebugObjectCommunicator
 * @package App\Models\Logger
 */
class DebugObjectCommunicator {

    public ?string $domain = null;

    public ?bool $debugModeEnabled = null;

    public ?bool $unauthorizedMode = null;

    public ?bool $freshUpdate = null;

    public ?string $presenterObjectClass = null;

    public ?string $userId = null;

    public ?string $languageISO = null;

    public ?string $bearerToken = null;
}

<?php

declare(strict_types=1);

namespace MasterApp\Networking;

/**
 * Class SendRequestObjectParameters
 * @package MasterApp\Networking;
 */
class SendRequestObjectParameters {

    // Rest Endpoint [Requested!]
    public string $endpoint;

    // Rest request entity
    public ?object $entity = null;

    // Rest request entities
    public ?array $entityList = null;

    /***************** cURL *****************/

    // Request Type [Default GET]
    public RequestTypes $type;

    // Override presenter language [Default presenter language]
    public ?string $languageISO = null;

    // CURL timeout [Default 10]
    public int $curlTimeout = 10;

    // Set content type of request [Default JSON]
    public string $contentType = 'application/json';

    // Don't send accept language header [Optional]
    public ?bool $dontSendLanguageInRequest = null;

    // Don't send info from request in headers [Optional]
    public ?bool $dontSendRequestInfoInRequest = null;

    /***************** Auth *****************/

    // Specific access token [Optional]
    public ?string $bearerToken = null;

    // Specify if you use bearer from user object in presenter [Optional]
    public bool $useBearerTokenFromPresenter = true;

    // Specify if you use basic auth from config [Optional]
    public bool $useBasicAuthFromConfig = false;

    // If we need to manually set user id
    public ?string $forceUserId = null;

    /***************** Cache *****************/

    // Valid seconds for the response [Default cache disabled]
    public ?int $validSeconds = null;

    // Invalidate this cache tags [Optional]
    public ?array $invalidateTags = null;

    // Invalidate this cache tags for current user [Optional]
    public ?array $invalidateUserTags = null;

    // If some extra tags used in cache [Optional]
    public ?array $additionalCacheTags = null;

    // Disable cache divided per user [Optional]
    public ?bool $disableCachePerUser = null;

    // Disable cache divided per language [Optional]
    public ?bool $disableCachePerLanguage = null;

    /***************** Data *****************/

    // Return result as raw string [Optional]
    public ?bool $returnRawDataAsString = null;

    // Ignore payload back from API [Optional]
    public ?bool $dontProcessResultData = null;

    // Regex terms which should be skipped from reporting as error [Optional]
    public array $ignoredErrorRegexes = [];

    // Don't try to process unauthorized [Optional]
    public ?bool $dontProcessUnauthorizedException = null;

    // Don't try to process bad request as exception [Optional]
    public ?bool $dontThrowExceptionOnBadRequest = null;

    public function __construct() {
        $this->type = RequestTypes::GET;
    }
}
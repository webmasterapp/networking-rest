<?php

declare(strict_types=1);

namespace MasterApp\Networking;
use ReflectionClass;
use ReflectionException;

abstract class BaseNetworking {

    public string $entitiesNamespace;

    /**
     * @param string $entitiesNamespace
     */
    public function setEntitiesNamespace(string $entitiesNamespace): void {
        $this->entitiesNamespace = $entitiesNamespace; }

    /**
     * @param string $class
     * @return array
     * @throws ReflectionException
     * @throws ConstructorWrongParameterDocCommentException
     * @throws ConstructorEmptyParameterDocCommentException
     */
    protected function getEntityParameters(string $class): array {

        $parameters = array_keys(get_class_vars($class));
        $reflectionClass = new ReflectionClass($class);
        $outputParameters = [];

        foreach ($parameters as $parameter) {

            $property = $reflectionClass->getProperty($parameter);
            $comment = $property->getDocComment();
            if ($comment === false) { throw new ConstructorEmptyParameterDocCommentException("Parameter {$parameter} of class {$class} has empty doc"); }
            $re = '/\/[*]+\s@var\s([a-zA-Z0-9\\\\]+)(<[a-zA-Z0-9]+>)?(\|null)?/m';
            preg_match_all($re, $comment, $matches, PREG_SET_ORDER);
            if (! isset($matches[0][1])) { throw new ConstructorWrongParameterDocCommentException("Parameter {$parameter} of class {$class} has wrong doc: {$comment}"); }

            $isBaseType = $matches[0][1] === 'int' || $matches[0][1] === 'string' || $matches[0][1] === 'float' || $matches[0][1] === 'array' || $matches[0][1] === 'bool';
            $isDateTime = $matches[0][1] === 'DateTimeObject';
            if ($isBaseType) { $propertyClass = $matches[0][1]; }
            else if ($isDateTime) { $propertyClass = __NAMESPACE__ . '\\' . $matches[0][1]; }
            else { $propertyClass = $this->entitiesNamespace . '\\' . $matches[0][1]; }
            $classToInit = isset($matches[0][2]) && $propertyClass === 'array' ? trim($matches[0][2], '<>') : null;

            $outputParameters[] = ['name' => $parameter, 'type' => $propertyClass, 'class' => $classToInit !== null ? $this->entitiesNamespace . '\\' . $classToInit : null];
        }

        return $outputParameters;
    }
}

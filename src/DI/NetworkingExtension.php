<?php

declare(strict_types=1);

namespace MasterApp\Networking\DI;
use MasterApp\Networking\Communicator;
use MasterApp\Networking\Constructor;
use MasterApp\Networking\Logger;
use MasterApp\Networking\TestCreator;
use Nette\DI\CompilerExtension;

/**
 * Class NetworkingExtension
 * @package MasterApp\Networking\DI
 */
class NetworkingExtension extends CompilerExtension {

    public function loadConfiguration(): void {

        // Init extension build
        $config = $this->getConfig();
        $builder = $this->getContainerBuilder();

        $builder->addDefinition($this->prefix('logger'))
            ->setType(Logger::class)
            ->addSetup('setSpikeTimeoutSeconds', [$config['spikeTimeoutSeconds']])
            ->addSetup('setIgnored', [$config['ignored'] ?? null])
            ->addSetup('setVersion', [$config['version']])
            ->addSetup('setEnvironment', [$config['environment']])
            ->addSetup('setPreciseLog', [$config['preciseLog'] ?? false])
            ->addSetup('setLogPath', [$config['logPath']]);

        foreach ($config['backends'] as $name => $backend) {
            $builder->addDefinition($this->prefix("communicator{$name}"))
                ->setType(Communicator::class)
                ->addSetup('setDomain', [$backend['apiUrl']])
                ->addSetup('setBasicAuth', [$backend['basicAuth'] ?? null])
                ->addSetup('setUnauthorizedLink', [$backend['unauthorizedLink']])
                ->addSetup('setLoginLink', [$backend['loginLink']])
                ->addSetup('setDebug', [$backend['debugMode'] ?? true])
                ->addSetup('setUnauthorizedMode', [$backend['unauthorizedMode'] ?? false])
                ->addSetup('setIgnoredRegexes', [$backend['ignoredRegexes'] ?? null]);

            // Creator
            $builder->addDefinition($this->prefix("testCreator{$name}"))
                ->setType(TestCreator::class)
                ->addSetup('setEntitiesNamespace', [$backend['entitiesNamespace']]);

            // Constructor
            $builder->addDefinition($this->prefix("constructor{$name}"))
                ->setType(Constructor::class)
                ->addSetup('setEntitiesNamespace', [$backend['entitiesNamespace']]);
        }
    }
}

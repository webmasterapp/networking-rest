<?php

declare(strict_types=1);

namespace MasterApp\Networking;

/**
 * Class RequestTypes
 * @package MasterApp\Networking
 */
enum RequestTypes: string {

    case POST = 'POST';

    case GET = 'GET';

    case DELETE = 'DELETE';

    case PUT = 'PUT';
}

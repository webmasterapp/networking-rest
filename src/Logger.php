<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace MasterApp\Networking;
use DateTime;
use JsonException;
use MasterApp\Mattermost\Mattermost;
use MasterApp\Mattermost\MattermostException;
use MasterApp\Networking\Exceptions\RestCommunicationSystemException;
use MasterApp\Networking\Exceptions\RestException;
use Nette\Caching\Cache;
use Nette\Caching\Storage;
use RuntimeException;
use function Sentry\captureException;
use Sentry\State\Scope;
use function Sentry\withScope;

/**
 * Class Logger
 * Service for the APIs
 * @author Martin Pavelka
 * @package App\Models\System
 */
class Logger {

    // Separator [CONST]
    private const Line = '----------------------------------------------------------------------------------------------------';

    // Tag for cache and spike protection
    private const spikeProtectionCacheTag = 'spikeProtection';

    // Directory for the logs [DI]
    public string $logPath;

    // App version [DI]
    private string $version;

    // Ignored outputs [DI]
    private ?array $ignored;

    // Environment name [DI]
    private string $environment;

    // If log everything [DI]
    private bool $preciseLog;

    // Seconds to message next exception
    private int $spikeTimeoutSeconds;
    public function __construct(private readonly Mattermost $mattermost, private readonly Storage $cache) {}

    public function setSpikeTimeoutSeconds(int $spikeTimeoutSeconds): void { $this->spikeTimeoutSeconds = $spikeTimeoutSeconds; }
    public function setLogPath(string $logPath): void { $this->logPath = $logPath; }
    public function setVersion(string $version): void { $this->version = $version; }
    public function setIgnored(?array $ignored): void { $this->ignored = $ignored; }
    public function setEnvironment(string $environment): void { $this->environment = $environment; }
    public function setPreciseLog(bool $preciseLog): void { $this->preciseLog = $preciseLog; }

    /**
     * @param RestException $exception
     * @throws RestCommunicationSystemException
     */
    public function generateLogFromException(RestException $exception): void {
        $this->generateLog($exception->debugObject, $exception);
    }

    /**
     * @param DebugObject $debugObject
     * @throws RestCommunicationSystemException
     */
    public function generateLogFromDebugObject(DebugObject $debugObject): void {
        if (! $this->preciseLog) { return; }
        $this->generateLog($debugObject);
    }

    /**
     * Initialize log directory
     * Creates dir if not exists
     */
    private function setUpLogDir(): void {

        $exceptions = "{$this->logPath}/exceptions";
        if (! file_exists($exceptions) && ! mkdir($exceptions, 0777, true) && ! is_dir($exceptions)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $exceptions));
        }

        $debug = "{$this->logPath}/debug";
        if (! file_exists($debug) && ! mkdir($debug, 0777, true) && ! is_dir($debug)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $debug));
        }
    }

    /**
     * Generate the curl log
     * @todo don't show request in test cases
     * @throws RestCommunicationSystemException
     */
    private function generateLog(DebugObject $debugObject, ?RestException $exception = null): void {

        $this->setUpLogDir();
        $exceptionMessage = $exception === null ? '' : $exception->getMessage();
        $exceptionCode = $exception === null ? '' : $exception->getCode();
        $header = "** :apierror: {$exceptionCode}: {$exceptionMessage} **\n";
        $header .= 'Time of log entry: ' . date('d/m/Y H:i:s') . "\n\n";

        $debugObjectParameters = $debugObject->parameters;
        $debugObjectRequest = $debugObject->request;
        $this->version = $this->version === '0' ? 'which is not set' : $this->version;

        $apiResponse = is_string($debugObject->curlDebug->result) ? $debugObject->curlDebug->result : 'Probably empty response.';
        $currentUrl = $debugObjectRequest !== null ? $debugObjectRequest->getUrl() : 'Current client location not available.';
        $currentIp = $debugObjectRequest !== null ? Utils::getClientIp($debugObjectRequest) : 'none';
        $currentCountry = $debugObjectRequest !== null ? Utils::getClientCountry($debugObjectRequest) : 'unknown';
        $clientId = $debugObject->identity !== null ? $debugObject->identity->getId() : 'anonymous';
        $endpoint = $debugObject->parameters !== null ? $debugObject->parameters->endpoint : 'none';

        $identityJson = $curlJson = $parametersJson = $cacheJson = $communicatorJson = $parsedErrors = $requestHeaders = $requestBody = null;
        try {
            $identityJson = $debugObject->identity === null ? 'Without identity' : json_encode($debugObject->identity->getData(), JSON_THROW_ON_ERROR);
            $curlJson = json_encode($debugObject->curlDebug, JSON_THROW_ON_ERROR);
            $parametersJson = json_encode($debugObjectParameters, JSON_THROW_ON_ERROR);
            $cacheJson = json_encode($debugObject->cacheDebug, JSON_THROW_ON_ERROR);
            $communicatorJson = json_encode($debugObject->communicatorDebug, JSON_THROW_ON_ERROR);
            $parsedErrors = $debugObject->errors === null ? 'Without parsed errors' : json_encode($debugObject->errors, JSON_THROW_ON_ERROR);
            $requestHeaders = $debugObjectRequest !== null ? json_encode($debugObjectRequest->getHeaders(), JSON_THROW_ON_ERROR) : 'Headers not available.';
            $requestBody = $debugObjectRequest?->getRawBody();
            $requestBody = $requestBody === null || $requestBody === '' ? 'Body not available.' : json_encode($requestBody, JSON_THROW_ON_ERROR);
        } catch (JsonException) {}

        // Init table
        $message = "| Type                   | Info                                                              |
                    | :--------------------- |:-----------------------------------------------------------------:|
                    | Environment            | {$this->environment} in version {$this->version}                      | 
                    | Current URL            | {$currentUrl}                                                       |
                    | Visitor info           | Client {$clientId} with IP {$currentIp} from {$currentCountry} country. |
                    | Rest endpoint          | {$endpoint}                                                         |
                    | API response           | `{$apiResponse}`                                                    |
                    | Input parameters       | `{$parametersJson}`                                                 |
                    | Parsed errors          | `{$parsedErrors}`                                                   |
                    | Communication debug    | `{$curlJson}`                                                       |
                    | Redis cache debug      | `{$cacheJson}`                                                      |
                    | Library debug          | `{$communicatorJson}`                                               |
                    | Identity debug         | `{$identityJson}`                                                   |
                    | Request headers        | `{$requestHeaders}`                                                 |
                    | Request body           | `{$requestBody}`                                                    |\n";

        // Init local
        $msgDebug = "Environment         \n   {$this->environment} in version {$this->version}                      \n\n";
        $msgDebug .= "Current URL         \n   {$currentUrl}                                                       \n\n";
        $msgDebug .= "Visitor info        \n   Client {$clientId} with IP {$currentIp} from {$currentCountry} country. \n\n";
        $msgDebug .= "Rest endpoint       \n   {$endpoint}                                                         \n\n";
        $msgDebug .= "API response        \n   {$apiResponse}                                                      \n\n";
        $msgDebug .= "Input parameters    \n   {$parametersJson}                                                   \n\n";
        $msgDebug .= "Parsed errors       \n   {$parsedErrors}                                                     \n\n";
        $msgDebug .= "Communication debug \n   {$curlJson}                                                         \n\n";
        $msgDebug .= "Redis cache debug   \n   {$cacheJson}                                                        \n\n";
        $msgDebug .= "Library debug       \n   {$communicatorJson}                                                 \n\n";
        $msgDebug .= "Identity debug      \n   {$identityJson}                                                     \n\n";
        $msgDebug .= "Request headers     \n   {$requestHeaders}                                                   \n\n";
        $msgDebug .= "Request body        \n   {$requestBody}                                                      \n\n";

        if ($exception !== null) { $this->saveExceptionToLocalStorage($header . $msgDebug); }
        else { $this->saveDebugObjectToLocalStorage($header . $msgDebug); }

        if ($exception !== null && ($this->exceptionIsIgnored($exception) || $this->isExceptionSpike($exception))) { return; }
        if ($exception === null) { return; }
        $this->sendExceptionToSentry($debugObject, $exception);
        try { $this->mattermost->sendMessage($header . $message); } catch (MattermostException $e) {
            throw new RestCommunicationSystemException($debugObject, 'Impossible to send log to Mattermost!', $e);
        }
    }

    /**
     * Save uniq exception into cache
     * If already saved skip it
     * @param RestException $exception
     * @return bool - found spike
     */
    private function isExceptionSpike(RestException $exception): bool {

        if ($exception->debugObject->parameters === null) { return false; }
        $query = $exception->debugObject->parameters->endpoint;
        $message = $exception->getMessage();
        $code = $exception->getCode();
        $uniqCacheKey = __DIR__ . "-{$query}-{$exception}-{$code}-{$message}";
        $exceptionData = $this->cache->read($uniqCacheKey);
        if ($exceptionData !== null) { return true; }
        $this->cache->write($uniqCacheKey, 's', [Cache::EXPIRE => $this->spikeTimeoutSeconds, Cache::TAGS => self::spikeProtectionCacheTag]);
        return false;
    }

    private function exceptionIsIgnored(RestException $exception): bool {

        if ($exception->debugObject->curlDebug->result !== null && is_string($exception->debugObject->curlDebug->result)) {
            if ($this->ignored === null) { return false; }
            foreach ($this->ignored as $message) {
                $matches = preg_match_all('/' . $message . '/', $exception->debugObject->curlDebug->result);
                if (is_int($matches) && $matches !== 0) { return true; }
            }
        }
        return false;
    }

    /**
     * Log exception into log directory
     * @param string $messageData
     */
    private function saveExceptionToLocalStorage(string $messageData): void {

        $message = $messageData . self::Line . self::Line . "\n\n";
        $message = str_replace(['|', '`'], ["\n", ''], $message);
        $fileName = (new DateTime())->format('Y-m-d') . '.txt';
        file_put_contents("{$this->logPath}/exceptions/{$fileName}", $message, FILE_APPEND);
    }

    /**
     * Log debug object into log directory
     * @param string $messageData
     */
    private function saveDebugObjectToLocalStorage(string $messageData): void {

        $message = $messageData . self::Line . self::Line . "\n\n";
        $message = str_replace(['|', '`'], ["\n", ''], $message);
        $fileName = (new DateTime())->format('Y-m-d') . '.txt';
        file_put_contents("{$this->logPath}/debug/{$fileName}", $message, FILE_APPEND);
    }

    /**
     * Send exception into sentry
     * @param DebugObject $debugObject
     * @param RestException $exception
     */
    private function sendExceptionToSentry(DebugObject $debugObject, RestException $exception): void {

        withScope(static function(Scope $scope) use ($debugObject, $exception) {

            if ($debugObject->identity !== null) {
                $scope->setUser([
                    'id' => $debugObject->identity->getId(),
                    'token' => $debugObject->identity->getRoles()[0] ?? null,
                    'data' => $debugObject->identity->getData(), ]);
            }

            $scope->setExtra('API response', $debugObject->curlDebug->resultArray);
            $scope->setExtra('Input parameters', $debugObject->parameters);
            $scope->setExtra('Parsed errors', $debugObject->errors);
            $scope->setExtra('Communication debug', $debugObject->curlDebug);
            $scope->setExtra('Redis cache debug', $debugObject->cacheDebug);
            $scope->setExtra('Library debug', $debugObject->communicatorDebug);
            $scope->setExtra('Identity debug', $debugObject->identity?->getData());
            $scope->setExtra('Request body', $debugObject->request?->getRawBody());
            captureException($exception);
        });
    }
}
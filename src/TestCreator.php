<?php

declare(strict_types=1);

namespace MasterApp\Networking;
use Exception;
use ReflectionException;

/**
 * Class TestCreator
 * @package GraphQLFrontApi
 */
class TestCreator extends BaseNetworking {

    /**
     * @param IBaseEntity $entity
     * @param string $location
     * @throws ConstructorEmptyParameterDocCommentException
     * @throws ConstructorWrongParameterDocCommentException
     * @throws ReflectionException
     * @throws Exception
     */
    public function processAssertions(IBaseEntity $entity, string $location = 'result'): void {

        $referenceObjectClass = get_class($entity);
        $referenceObjectParameters = $this->getEntityParameters($referenceObjectClass);

        foreach ($referenceObjectParameters as $parameter) {

            // Null
            $name = $parameter['name'];
            $newLocation = $location . '->' . $name;
            if ($entity->{$name} === null) { continue; }

            // Array
            if ($parameter['type'] === 'array' && ! empty($entity->{$name})) {
                $this->processAssertions($entity->{$name}[0], $location . '->' . $name . '[0]');
            }

            // Entity
            if (is_subclass_of($parameter['type'], IBaseEntity::class)) {
                $this->processAssertions($entity->{$name}, $newLocation);
            }

            // Other
            $this->printAssertion($newLocation, $parameter['type']);
        }
    }

    /**
     * @param array $list
     * @param string $location
     * @throws ConstructorEmptyParameterDocCommentException
     * @throws ConstructorWrongParameterDocCommentException
     * @throws ReflectionException
     * @throws Exception
     */
    public function processAssertionsFromCollection(array $list, string $location = 'result'): void {

        if (empty($list)) { return; }
        $this->processAssertions($list[0], $location . '[0]');
        $this->printAssertion($location, 'array');
    }

    /**
     * @param string $location
     * @param string $type
     * @throws Exception
     */
    private function printAssertion(string $location, string $type): void {

        $randomIdentifier = self::randHash();
        if ($type === 'double') { $type = 'float'; }
        echo "Assert::type('{$type}', \${$location}, '{$randomIdentifier}');\n";
    }

    /**
     * @return string
     * @throws Exception
     */
    private static function randHash(): string {
        return substr(md5(random_bytes(20)),-(32));
    }
}

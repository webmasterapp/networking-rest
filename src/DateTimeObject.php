<?php

declare(strict_types=1);

namespace MasterApp\Networking;
use DateTime;
use Exception;

/**
 * Class DateTimeObject
 * @package MasterApp\Networking
 */
class DateTimeObject extends DateTime {

    /**
     * Convert datetime into custom graphql date
     * @param string $dateTime
     * @return DateTimeObject
     * @throws Exception
     */
    public static function buildFromString(string $dateTime): self {

        $normalDate = explode('.', $dateTime);
        $instance = DateTime::createFromFormat('Y-m-d\TH:i:s', $normalDate[0]);
        if ($instance === false) { throw new DateTimeObjectBuildFailedException("Can not build date from: {$dateTime}"); }
        $converted = new self();
        $converted->setTimestamp($instance->getTimestamp());
        return $converted;
    }
}

/**
 * Class DateTimeObjectBuildFailedException
 * @package MasterApp\Networking
 */
class DateTimeObjectBuildFailedException extends Exception {}
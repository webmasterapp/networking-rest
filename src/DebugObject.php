<?php

declare(strict_types=1);

namespace MasterApp\Networking;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use MasterApp\Networking\Exceptions\ApiParsedError;
use Nette\Http\IRequest;
use Nette\Security\IIdentity;

/**
 * Class DebugObject
 * @package App\Models\Logger
 */
class DebugObject {

    // Input variable parameters debug
    public ?SendRequestObjectParameters $parameters = null;

    // Request debug
    public ?IRequest $request = null;

    // CURL debug
    public DebugObjectCurl $curlDebug;

    // Class debug
    public DebugObjectCommunicator $communicatorDebug;

    // Cache debug
    public DebugObjectCache $cacheDebug;

    // User debug
    public ?IIdentity $identity = null;

    // Logger object
    public ?Logger $logger = null;

    // Api errors
    #[ArrayShape([ApiParsedError::class])] public ?array $errors = null;

    #[Pure] public function __construct() {

        $this->curlDebug = new DebugObjectCurl();
        $this->communicatorDebug = new DebugObjectCommunicator();
        $this->cacheDebug = new DebugObjectCache();
    }
}

<?php

declare(strict_types=1);

namespace MasterApp\Networking;

/**
 * Interface IBaseEntity
 * @package MasterApp\Networking
 */
interface IBaseEntity {}
<?php

declare(strict_types=1);

namespace MasterApp\Networking\Exceptions;
use MasterApp\Networking\DebugObject;
use Throwable;

/**
 * Class RestCommunicationSystemException
 * @package MasterApp\Networking\Exceptions
 */
class RestCommunicationSystemException extends RestException {

    public function __construct(DebugObject $debugObject, string $message, ?Throwable $previous = null) {
        parent::__construct($debugObject, $previous, $message, 500);
    }
}
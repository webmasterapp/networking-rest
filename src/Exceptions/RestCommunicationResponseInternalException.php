<?php

declare(strict_types=1);

namespace MasterApp\Networking\Exceptions;

/**
 * Class RestCommunicationResponseInternalException
 * @package MasterApp\Networking\Exceptions
 */
class RestCommunicationResponseInternalException extends RestException {
}
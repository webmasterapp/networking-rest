<?php

declare(strict_types=1);

namespace MasterApp\Networking\Exceptions;

use MasterApp\Networking\DebugObject;
use Throwable;

/**
 * Class RestCommunicationResponseUnauthorizedException
 * @package MasterApp\Networking\Exceptions
 */
class RestCommunicationResponseUnauthorizedException extends RestException {

    public function __construct(DebugObject $debugObject, Throwable $previous = null) {
        parent::__construct($debugObject, $previous);
    }
}
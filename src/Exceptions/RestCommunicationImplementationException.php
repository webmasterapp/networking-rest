<?php

declare(strict_types=1);

namespace MasterApp\Networking\Exceptions;

use MasterApp\Networking\DebugObject;
use Throwable;

/**
 * Class RestCommunicationImplementationException
 * @package MasterApp\Networking\Exceptions
 */
class RestCommunicationImplementationException extends RestException {

    public function __construct(DebugObject $debugObject, string $message = null, ?Throwable $previous = null) {
        parent::__construct($debugObject, $previous, $message, 501);
    }
}
<?php

declare(strict_types=1);

namespace MasterApp\Networking\Exceptions;
use Exception;
use JetBrains\PhpStorm\ArrayShape;
use MasterApp\Networking\DebugObject;
use RuntimeException;
use Throwable;
use Tracy\Debugger;

/**
 * Class RestException
 * @package MasterApp\Networking\Exceptions
 */
abstract class RestException extends Exception {

    #[ArrayShape([ApiParsedError::class])] public ?array $errors = null;

    private ?array $apiResponse;

    /**
     * @throws RestCommunicationSystemException
     * @throws Exception
     */
    public function __construct(
        public DebugObject $debugObject,
        private readonly ?Throwable $previous = null,
        ?string $message = null,
        ?int $code = null) {

        $this->apiResponse = $debugObject->curlDebug->resultArray;
        $this->parseErrorSimple();
        parent::__construct($message ?? $this->getExceptionMessage(), $code ?? $this->getExceptionCode(), $previous);
        Debugger::barDump($this->debugObject, $this->debugObject->parameters !== null ? $this->debugObject->parameters->endpoint : 'Missing endpoint.');

        if ($this->debugObject->logger === null) { throw new RuntimeException('Logger not referenced in DO.'); }
        $this->debugObject->logger->generateLogFromException($this);
    }

    /**
     * Try to get some exception code
     * @return int
     */
    private function getExceptionCode(): int {

        $previousExceptionCode = $this->previous?->getCode();
        $curlCode = $this->debugObject->curlDebug->code !== 200 ? $this->debugObject->curlDebug->code : null;
        return $curlCode ?? $this->errors[0]->code ?? $previousExceptionCode ?? 400;
    }

    /**
     * Try to find some exceptions message
     * @return string
     */
    private function getExceptionMessage(): string {

        $callerFunctionName = ucfirst(debug_backtrace()[5]['function']);
        $totalErrors = $this->errors === null ? 0 : count($this->errors);
        if ($this->errors === null) { return "{$callerFunctionName} throws exception! Please see debug object."; }
        if (count($this->errors) > 1) { return "{$callerFunctionName} throws exception with {$totalErrors} errors! Please see debug object."; }

        return $this->errors[0]->message
            ?? $this->errors[0]->exception
            ?? $this->errors[0]->error
            ?? $this->errors[0]->path
            ?? "{$callerFunctionName} throws exception without message! Please see debug object.";
    }

    private function parseErrorSimple(): void {

        if (! isset($this->apiResponse['error']) && ! isset($this->apiResponse['exception'])) { return; }
        $error = new ApiParsedError();
        $error->code = $this->apiResponse['code'] ?? null;
        $error->error = $this->apiResponse['error'] ?? null;
        $error->exception = $this->apiResponse['exception'] ?? null;
        $error->message = $this->apiResponse['message'] ?? null;
        $error->path = $this->apiResponse['path'] ?? null;
        $error->status = $this->apiResponse['status'] ?? null;
        $error->timestamp = $this->apiResponse['timestamp'] ?? null;
        $this->errors[] = $error;
        $this->debugObject->errors = $this->errors;
    }
}

/**
 * Class ApiParsedError
 * @package MasterApp\Networking\Exceptions
 */
class ApiParsedError {

    public ?int $code = null;

    public ?string $error = null;

    public ?string $exception = null;

    public ?string $message = null;

    public ?string $path = null;

    public ?int $status = null;

    public ?string $timestamp = null;
}
<?php

declare(strict_types=1);

namespace MasterApp\Networking\Exceptions;

use MasterApp\Networking\DebugObject;
use Throwable;

/**
 * Class RestCommunicationResponseWarningException
 * @package MasterApp\Networking\Exceptions
 */
class RestCommunicationResponseWarningException extends RestException {

    public function __construct(DebugObject $debugObject, Throwable $previous = null) {
        parent::__construct($debugObject, $previous);
    }

    public function validationErrorPresented(string $validationError): bool {

        if ($this->errors === null) { return false; }
        foreach ($this->errors as $error) {
            if ($error->message === $validationError) { return true; }
            if ($error->exception === $validationError) { return true; }
            if ($error->error === $validationError) { return true; }
            if ($error->path === $validationError) { return true; }
        } return false;
    }
}
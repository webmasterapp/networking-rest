<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace MasterApp\Networking;
use Exception;
use ReflectionException;
use stdClass;

/**
 * Class Constructor
 * @package MasterApp\Networking
 */
class Constructor extends BaseNetworking {

    /**
     * @param array $payload
     * @param string $class
     * @return array
     * @throws ConstructorWrongParameterDocCommentException
     * @throws ReflectionException
     */
    public function constructCollectionFromPayload(array $payload, string $class): array {

        $constructionArray = [];
        foreach ($payload as $value) {
            $constructionItem = new $class();
            $this->constructEntityFromPayload($value, $constructionItem);
            $constructionArray[] = $constructionItem;
        }

        return $constructionArray;
    }

    /**
     * @param array $payload
     * @param object $constructionObject
     * @throws ConstructorWrongParameterDocCommentException
     * @throws ReflectionException
     * @throws Exception
     */
    public function constructEntityFromPayload(array $payload, object $constructionObject): void {

        $classToInitialize = get_class($constructionObject);
        $constructionObjectParameters = $this->getEntityParameters($classToInitialize);

        foreach ($constructionObjectParameters as $parameter) {

            // Not received
            $name = $parameter['name'];
            if (! isset($payload[$name])) {
                $constructionObject->{$name} = null;
                continue;
            }

            // Array
            if ($parameter['type'] === 'array') {

                $constructionObject->{$name} = [];
                foreach ($payload[$parameter['name']] as $entityToInitialize) {

                    if (is_string($entityToInitialize) || is_int($entityToInitialize) ||
                        is_bool($entityToInitialize) || is_float($entityToInitialize)) {
                        $constructionObject->{$name}[] = $entityToInitialize;
                        continue;
                    }

                    if (! class_exists($parameter['class'])) {
                        $constructionObject->{$name}[] = $entityToInitialize;
                        continue;
                    }

                    $subObjectToInitialize = new $parameter['class']();
                    $this->constructEntityFromPayload($entityToInitialize, $subObjectToInitialize);
                    $constructionObject->{$name}[] = $subObjectToInitialize;
                }
                continue;
            }

            // DateTime
            /** @noinspection ClassConstantCanBeUsedInspection */
            if ($parameter['type'] === __NAMESPACE__ . '\\DateTimeObject') {
                $constructionObject->{$name} = DateTimeObject::buildFromString($payload[$name]);
                continue;
            }

            // Enum
            if (enum_exists($parameter['type'])) {
                $constructionObject->{$name} = $parameter['type']::from($payload[$name]);
                continue;
            }

            // Dto object
            if (is_subclass_of($parameter['type'], IBaseEntity::class)) {
                $objectToInitialize = new $parameter['type']();
                $this->constructEntityFromPayload($payload[$name], $objectToInitialize);
                $constructionObject->{$name} = $objectToInitialize;
                continue;
            }

            $constructionObject->{$name} = $payload[$name];
        }
    }

    /**
     * @param IBaseEntity $entity
     * @return object
     * @throws ConstructorWrongParameterDocCommentException
     * @throws ReflectionException
     * @throws ConstructorEmptyParameterDocCommentException
     */
    public function preparePayloadFromEntity(IBaseEntity $entity): object {

        $constructionObject = new stdClass();
        $referenceObjectClass = get_class($entity);
        $referenceObjectParameters = $this->getEntityParameters($referenceObjectClass);

        foreach ($referenceObjectParameters as $parameter) {

            // Null
            $name = $parameter['name'];
            if ($entity->{$name} === null) {
                continue;
            }

            // Array
            if ($parameter['type'] === 'array') {
                $constructionObject->{$name} = [];
                foreach ($entity->{$name} as $entityToInitialize) {
                    if (is_string($entityToInitialize) || is_int($entityToInitialize) ||
                        is_bool($entityToInitialize) || is_float($entityToInitialize)) {
                        $constructionObject->{$name}[] = $entityToInitialize;
                        continue;
                    }
                    $constructionObject->{$name}[] = $this->preparePayloadFromEntity($entityToInitialize);
                }
                continue;
            }

            // Entity
            if (is_subclass_of($parameter['type'], IBaseEntity::class)) {
                $constructionObject->{$name} = $this->preparePayloadFromEntity($entity->{$name});
                continue;
            }

            // Other
            $constructionObject->{$name} = $entity->{$name};
        }

        return $constructionObject;
    }
}

/**
 * Class ConstructorWrongParameterDocCommentException
 * @package MasterApp\Networking
 */
class ConstructorWrongParameterDocCommentException extends Exception {}

/**
 * Class ConstructorEmptyParameterDocCommentException
 * @package MasterApp\Networking
 */
class ConstructorEmptyParameterDocCommentException extends Exception {}

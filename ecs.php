<?php

declare(strict_types=1);

use PhpCsFixer\Fixer\ClassNotation\ClassAttributesSeparationFixer;
use PhpCsFixer\Fixer\ClassNotation\ClassDefinitionFixer;
use PhpCsFixer\Fixer\ClassNotation\NoBlankLinesAfterClassOpeningFixer;
use PhpCsFixer\Fixer\FunctionNotation\MethodArgumentSpaceFixer;
use PhpCsFixer\Fixer\Phpdoc\GeneralPhpdocAnnotationRemoveFixer;
use PhpCsFixer\Fixer\Phpdoc\NoSuperfluousPhpdocTagsFixer;
use PhpCsFixer\Fixer\Phpdoc\PhpdocLineSpanFixer;
use PhpCsFixer\Fixer\PhpUnit\PhpUnitSetUpTearDownVisibilityFixer;
use PhpCsFixer\Fixer\Whitespace\MethodChainingIndentationFixer;
use Symplify\CodingStandard\Fixer\ArrayNotation\ArrayListItemNewlineFixer;
use Symplify\CodingStandard\Fixer\ArrayNotation\ArrayOpenerAndCloserNewlineFixer;
use Symplify\CodingStandard\Fixer\ArrayNotation\StandaloneLineInMultilineArrayFixer;
use Symplify\CodingStandard\Fixer\Commenting\RemoveUselessDefaultCommentFixer;
use Symplify\CodingStandard\Fixer\LineLength\DocBlockLineLengthFixer;
use Symplify\CodingStandard\Fixer\LineLength\LineLengthFixer;
use Symplify\CodingStandard\Fixer\Spacing\MethodChainingNewlineFixer;
use Symplify\CodingStandard\Fixer\Spacing\StandaloneLinePromotedPropertyFixer;
use Symplify\EasyCodingStandard\Config\ECSConfig;
use Symplify\EasyCodingStandard\ValueObject\Set\SetList;

return static function (ECSConfig $ecsConfig): void {

    $ecsConfig->paths([ __DIR__ . '/src', ]);

    $ecsConfig->skip([
        __DIR__ . '/app/Bootstrap.php',
        RemoveUselessDefaultCommentFixer::class => null,
        ClassDefinitionFixer::class => null,
        NoSuperfluousPhpdocTagsFixer::class => null,
        PhpdocLineSpanFixer::class => null,
        PhpUnitSetUpTearDownVisibilityFixer::class => null,
        LineLengthFixer::class => null,
        NoBlankLinesAfterClassOpeningFixer::class => null,
        MethodChainingIndentationFixer::class => null,
        MethodChainingNewlineFixer::class => null,
        ClassAttributesSeparationFixer::class => null,
        ArrayListItemNewlineFixer::class => null,
        ArrayOpenerAndCloserNewlineFixer::class => null,
        StandaloneLineInMultilineArrayFixer::class => null,
        DocBlockLineLengthFixer::class => null,
        StandaloneLinePromotedPropertyFixer::class => null,
        MethodArgumentSpaceFixer::class => null,
        GeneralPhpdocAnnotationRemoveFixer::class => null
    ]);

    $ecsConfig->sets([
        SetList::CLEAN_CODE,
        SetList::NAMESPACES,
        SetList::STRICT,
        SetList::SPACES,
        SetList::COMMON,
        SetList::SYMPLIFY,
        SetList::DOCBLOCK,
        SetList::CONTROL_STRUCTURES,
        SetList::ARRAY
    ]);
};